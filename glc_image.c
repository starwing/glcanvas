#include "glc_image.h"

typedef void image_deletor(GLC_Image image);

struct GLC_ImageObject {
    int texture_id;
    int width, height;
    image_deletor *deletor;
    void *data;
};

GLC_Image glc_image_new(int width, int height) {
    GLC_Image img = (GLC_Image)malloc(sizeof(GLC_ImageObject) + width * height * 4);
    if (!img) return NULL;
    img->texture_id = 0;
    img->width = img->height = 0;
    img->deletor = NULL;
    img->data = (void*)(img + 1);
    return img;
}

void glc_image_delete(GLC_Image image) {
    if (img->texture_id)
        glDeleteTexture(img->texture_id);
    if (deletor) img->deletor(img);
    free(img);
}

int glc_image_width(GLC_Image image) {
    return image->width;
}

int glc_image_height(GLC_Image image) {
    return image->height;
}

const void *glc_image_data(GLC_Image image) {
    return image->data;
}

void *glc_image_lock(GLC_Image image) {
    return image->data;
}

void glc_image_unlock(GLC_Image image) {
}

int  glc_pattern_image(struct GLCanvas *c, GLC_Image image);
void glc_drawimage(struct GLCanvas *c, GLC_Image image);

GLC_Image glc_image_from_file(const char *filename);
GLC_Image glc_image_from_data(const void *p, size_t len);


