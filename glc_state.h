#ifndef glc_state_h
#define glc_state_h


#include <stddef.h>


typedef struct GLC_State GLC_State;

typedef enum GLC_BlendMode {
    GLC_BLEND_SRC = 1,
    GLC_BLEND_SRC_IN,
    GLC_BLEND_DST_IN,
    GLC_BLEND_SRC_OUT,
    GLC_BLEND_DST_OUT,
    GLC_BLEND_SRC_ATOP,
    GLC_BLEND_DST_ATOP,
    GLC_BLEND_DST_OVER,
    GLC_BLEND_SRC_OVER,
    GLC_BLEND_MAX_ENUM
} GLC_BlendMode;

typedef enum GLC_MaskMode {
    GLC_MASK_UNION = 1,
    GLC_MASK_INTERSECT,
    GLC_MASK_SUBTRACT,
    GLC_MASK_XOR,
    GLC_MASK_MAX_ENUM
} GLC_MaskMode;


GLC_State *glc_new(int width, int height);
void       glc_delete(GLC_State *c);

void glc_reset(GLC_State *c, int width, int height);
void glc_clear(GLC_State *c);

int  glc_pushstate(GLC_State *c);
void glc_resetstate(GLC_State *c);
void glc_popstate(GLC_State *c);

void glc_setblendmode(GLC_State *c, GLC_BlendMode blendmode);
void glc_getblendmode(GLC_State *c, GLC_BlendMode *blendmode);

void glc_useclipbox(GLC_State *c, int enable);
int  glc_getclipbox(GLC_State *c, int rect[4]);
void glc_setclipbox(GLC_State *c, int rect[4]);

void glc_setcolor(GLC_State *c, float color[4]);
void glc_getcolor(GLC_State *c, float color[4]);
void glc_setclearcolor(GLC_State *c, float color[4]);
void glc_getclearcolor(GLC_State *c, float color[4]);

int  glc_pushmatrix(GLC_State *c);
void glc_resetmatrix(GLC_State *c);
void glc_popmatrix(GLC_State *c);
void glc_setmatrix(GLC_State *c, float mat[9]);
void glc_getmatrix(GLC_State *c, float mat[9]);
void glc_translate(GLC_State *c, float dx, float dy);
void glc_rotate(GLC_State *c, float angle);
void glc_scale(GLC_State *c, float sx, float sy);
void glc_shear(GLC_State *c, float kx, float ky);

void glc_begin (GLC_State *c);
void glc_end   (GLC_State *c);

void glc_triangles(GLC_State *c, size_t count, float *pos);
void glc_triangle_strip(GLC_State *c, size_t count, float *pos);
void glc_triangle_fan(GLC_State *c, size_t count, float *pos);
void glc_rect(GLC_State *c, float pos[4]);

#endif /* glc_state_h */
