#include "glc_matrix.h"


#include <math.h>


/* 
 * Matrix layout:
 * | SX KX TX |
 * | KY SY TY |
 * | W0 W1 W2 |
 */
enum { SX, KY, W0, KX, SY, W1, TX, TY, W2 };


/* basic matrix routines */

#define invalid_matrix(m) { \
    m[SX] = m[KY] = m[W0] = \
    m[KX] = m[SY] = m[W1] = \
    m[TX] = m[TY] = m[W2] = 0.f; }

#define set_matrix(m, sx, kx, tx, sy, ky, ty, w0, w1, w2) { \
    m[SX] = sx; m[KY] = ky; m[W0] = w0; \
    m[KX] = kx; m[SY] = sy; m[W1] = w1; \
    m[TX] = tx; m[TY] = ty; m[W2] = w2; }

#define copy_matrix(m1, m2) { \
    m1[SX] = m2[SX]; m1[KY] = m2[KY]; m1[W0] = m2[W0]; \
    m1[KX] = m2[KX]; m1[SY] = m2[SY]; m1[W1] = m2[W1]; \
    m1[TX] = m2[TX]; m1[TY] = m2[TY]; m1[W2] = m2[W2]; }

void glc_matrix_load_identity(float m[9]) {
    set_matrix(m,
            1.f, 0.f, 0.f,
            0.f, 1.f, 0.f,
            0.f, 0.f, 1.f);
}

void glc_matrix_load_matrix(float m[9], const float a[9]) {
    copy_matrix(m, a);
}

void glc_matrix_load_multiply(float res[9], const float a[9], const float b[9]) {
    res[SX] = a[SX]*b[SX] + a[KX]*b[KY] + a[TX]*b[W0];
    res[KY] = a[KY]*b[SX] + a[SY]*b[KY] + a[TY]*b[W0];
    res[W0] = a[W0]*b[SX] + a[W1]*b[KY] + a[W2]*b[W0];
    res[KX] = a[SX]*b[KX] + a[KX]*b[SY] + a[TX]*b[W1];
    res[SY] = a[KY]*b[KX] + a[SY]*b[SY] + a[TY]*b[W1];
    res[W1] = a[W0]*b[KX] + a[W1]*b[SY] + a[W2]*b[W1];
    res[TX] = a[SX]*b[TX] + a[KX]*b[TY] + a[TX]*b[W2];
    res[TY] = a[KY]*b[TX] + a[SY]*b[TY] + a[TY]*b[W2];
    res[W2] = a[W0]*b[TX] + a[W1]*b[TY] + a[W2]*b[W2];
}

void glc_matrix_load_translate(float m[9], float x, float y) {
    set_matrix(m,
            1.f, 0.f, x,
            0.f, 1.f, y,
            0.f, 0.f, 1.f);
}

void glc_matrix_load_rotate(float m[9], float angle) {
    float ca = (float)cos(angle);
    float sa = (float)sin(angle);
    set_matrix(m,
            ca, -sa,  0.f,
            sa,  ca,  0.f,
            0.f, 0.f, 1.f);
}

void glc_matrix_load_scale(float m[9], float sx, float sy) {
    set_matrix(m,
            sx,  0.f, 0.f,
            0.f, sy,  0.f,
            0.f, 0.f, 1.f);
}

void glc_matrix_load_shear(float m[9], float kx, float ky) {
    set_matrix(m,
            1.f, kx,  0.f,
            ky,  1.f, 0.f,
            0.f, 0.f, 1.f);
}


/* complicate matrix routines */

int glc_matrix_load_invert(float m[9], const float a[9]) {
    float d0 = a[SY]*a[W2] - a[W1]*a[TY];
    float d1 = a[W0]*a[TY] - a[KY]*a[W2];
    float d2 = a[KY]*a[W1] - a[W0]*a[SY];
    float d  = a[SX]*d0 + a[KX]*d1 + a[TX]*d2;
    if(d == 0.f) {
        invalid_matrix(m);
        return 0;
    }
    d = 1.f / d;
    m[SX] = d * d0;
    m[KY] = d * d1;
    m[W0] = d * d2;
    m[KX] = d * (a[W1]*a[TX] - a[KX]*a[W2]);
    m[SY] = d * (a[SX]*a[W2] - a[W0]*a[TX]);
    m[W1] = d * (a[W0]*a[KX] - a[SX]*a[W1]);
    m[TX] = d * (a[KX]*a[TY] - a[SY]*a[TX]);
    m[TY] = d * (a[KY]*a[TX] - a[SX]*a[TY]);
    m[W2] = d * (a[SX]*a[SY] - a[KY]*a[KX]);
    return 1;
}

int glc_matrix_load_square_to_quad(float m[9], const float q[8]) {
    float dx = q[0] - q[2] + q[4] - q[6];
    float dy = q[1] - q[3] + q[5] - q[7];
    if(dx == 0.f && dy == 0.f)
    {   
        /* Affine case (parallelogram)
         *---------------*/
        m[SX] = q[2] - q[0];
        m[KY] = q[3] - q[1];
        m[W0] = 0.f;
        m[KX] = q[4] - q[2];
        m[SY] = q[5] - q[3];
        m[W1] = 0.f;
        m[TX] = q[0];
        m[TY] = q[1];
        m[W2] = 1.f;
    }
    else
    {
        float dx1 = q[2] - q[4];
        float dy1 = q[3] - q[5];
        float dx2 = q[6] - q[4];
        float dy2 = q[7] - q[5];
        float den = dx1 * dy2 - dx2 * dy1;
        if(den == 0.f)
        {
            // Singular case
            //---------------
            invalid_matrix(m);
            return 0;
        }
        // General case
        //---------------
        float u = (dx * dy2 - dy * dx2) / den;
        float v = (dy * dx1 - dx * dy1) / den;
        m[SX] = q[2] - q[0] + u * q[2];
        m[KY] = q[3] - q[1] + u * q[3];
        m[W0] = u;
        m[KX] = q[6] - q[0] + v * q[6];
        m[SY] = q[7] - q[1] + v * q[7];
        m[W1] = v;
        m[TX] = q[0];
        m[TY] = q[1];
        m[W2] = 1.f;
    }
    return 1;
}

int glc_matrix_load_quad_to_square(float m[9], const float q[8]) {
    float temp[9];
    if (glc_matrix_load_square_to_quad(temp, q))
        return glc_matrix_load_invert(m, temp);
    return 0;
}

int glc_matrix_load_quad_to_quad(float m[9], const float from[8], const float to[8]) {
    float temp[9];
    if (!glc_matrix_load_quad_to_square(m, from)) return 0;
    if (!glc_matrix_load_square_to_quad(temp, to)) return 0;
    glc_matrix_multiply(m, temp);
    return 1;
}


/* matrix modify routines */

int glc_matrix_invert(float m[9]) {
    float temp[9];
    copy_matrix(temp, m);
    return glc_matrix_load_invert(m, temp);
}

void glc_matrix_multiply(float m[9], const float left[9]) {
    float right[9];
    copy_matrix(right, m);
    glc_matrix_load_multiply(m, left, right);
}

void glc_matrix_translate(float m[9], float x, float y) {
    m[TX] += x;
    m[TY] += y;
}

void glc_matrix_rotate(float m[9], float angle) {
    float temp[9];
    glc_matrix_load_rotate(temp, angle);
    glc_matrix_multiply(m, temp);
}

void glc_matrix_scale(float m[9], float sx, float sy) {
    float temp[9];
    glc_matrix_load_scale(temp, sx, sy);
    glc_matrix_multiply(m, temp);
}

void glc_matrix_shear(float m[9], float kx, float ky) {
    float temp[9];
    glc_matrix_load_shear(temp, kx, ky);
    glc_matrix_multiply(m, temp);
}


/* matrix transform routines */

void glc_matrix_transform(const float m[9], size_t pointcount, float *v) {
    enum { X, Y };
    while (pointcount--) {
        float x = v[X];
        float y = v[Y];
        float d = 1.f / (x*m[W0] + y*m[W1] + m[W2]);
        v[X] = d * (x*m[SX] + y*m[KX] + m[TX]);
        v[Y] = d * (x*m[KY] + y*m[SY] + m[TY]);
        v += 2;
    }
}

void glc_matrix_transform_affline(const float m[9], size_t pointcount, float *v) {
    enum { X, Y };
    while (pointcount--) {
        float x = v[X];
        float y = v[Y];
        v[X] = x*m[SX] + y*m[KX] + m[TX];
        v[Y] = x*m[KY] + y*m[SY] + m[TY];
        v += 2;
    }
}

int glc_matrix_transform_invert(const float m[9], size_t pointcount, float *v) {
    float inv[9];
    if (glc_matrix_load_invert(inv, m)) {
        glc_matrix_transform(inv, pointcount, v);
        return 1;
    }
    return 0;
}

int glc_matrix_transform_invert_affline(const float m[9], size_t pointcount, float *v) {
    enum { X, Y };
    float d = 1.f / (m[SX] * m[SY] - m[KY] * m[KX]);
    while (pointcount--) {
        float a = (v[X] - m[TX]) * d;
        float b = (v[Y] - m[TY]) * d;
        v[X] = a * m[SY] - b * m[KX];
        v[Y] = b * m[SX] - a * m[KY];
        v += 2;
    }
    return 1;
}


