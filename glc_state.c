#include "glc_state.h"
#include "glc_matrix.h"


#include <GL/GL.h>
#include <stdlib.h>


typedef struct StackNode {
    struct StackNode *prev;
    struct StackNode *next;
} StackNode;

typedef struct GLC_Config {
    StackNode node;
    int mask_enable;
    int clipbox_enable, clipbox[4];
    float color[4], clearcolor[4];
    GLC_BlendMode  blendmode;
} GLC_Config;

typedef struct GLC_Matrix {
    StackNode node;
    float mat[9];
} GLC_Matrix;

struct GLC_State {
    int width, height;

    GLC_Config *state;
    GLC_Config  top_state;

    GLC_Matrix *matrix;
    GLC_Matrix  top_matrix;
};


/* context stack maintains */

static void init_context(StackNode *base) {
    base->prev = base->next = NULL;
}

static void free_context(StackNode *base) {
    while (base != NULL) {
        StackNode *next = base->next;
        free(base);
        base = next;
    }
}

static StackNode *push_context_(StackNode *cur, size_t objsize) {
    if (cur->next != NULL) {
        StackNode next = *cur->next;
        memcpy(cur->next, cur, objsize);
        memcpy(cur->next, &next, sizeof(StackNode));
    }
    else {
        StackNode *next = malloc(objsize);
        if (next == NULL) return NULL;
        memcpy(next, cur, sizeof(objsize));
        next->prev = cur;
        next->next = NULL;
        cur->next = next;
    }
    return cur->next;
}

#define push_context(cur,type) do { \
    type *new_node = (type*)push_context_(&(cur)->node,sizeof(type)); \
    if (new_node == NULL) return 0; \
    (cur) = new_node; } while (0);

#define pop_context(cur,type) do { \
    if ((cur)->node.prev == NULL) return; \
    (cur) = (type*)(cur)->node.prev; } while (0)


/* canvas creation */

GLC_State *glc_new(int width, int height) {
    GLC_State *c = malloc(sizeof(GLC_State));
    if (c == NULL) return NULL;
    c->state = &c->top_state;
    c->matrix = &c->top_matrix;
    init_context(&c->top_state.node);
    init_context(&c->top_matrix.node);
    glc_reset(c, width, height);
    return c;
}

void glc_delete(GLC_State *c) {
    free_context(c->top_state.node.next);
    free_context(c->top_matrix.node.next);
    c->state = &c->top_state;
    c->matrix = &c->top_matrix;
    /* free GLC_State */
    free(c);
}

void glc_reset(GLC_State *c, int width, int height) {
    if (width > 0) c->width = width;
    if (height > 0) c->height = height;

    /* setup OpenGL */

    /* Enable blending */
    glEnable(GL_BLEND);

    /* "Normal" blending */
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    /* Disable textures */
    glDisable(GL_TEXTURE_2D);

    /* Set the viewport to top-left corner */
    glViewport(0, 0, width, height);

    /* Reset the projection matrix */
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    /* Set up orthographic view (no depth) */
    glOrtho(0.0, width, height,0.0, -1.0, 1.0);

    /* Reset modelview matrix */
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    /* Set pixel row alignment */
    glPixelStorei(GL_UNPACK_ALIGNMENT, 2);

    /* reset state */
    glc_resetstate(c);
}

void glc_clear(GLC_State *c) {
    glClear(GL_COLOR_BUFFER_BIT);
}


/* canvas state maintains */

static void init_state(GLC_Config *state) {
    float *fg = state->color, *bg = state->clearcolor;
    state->clipbox_enable = 0;
    state->mask_enable = 0;
    state->blendmode = GLC_BLEND_SRC_OVER;
    fg[0] = fg[1] = fg[2] = fg[3] = 1.0f;
    bg[0] = bg[1] = bg[2] = bg[3] = 1.0f;
}

static void flush_state(GLC_State *c, GLC_Config *state) {
    state->clipbox_enable = glc_getclipbox(c, state->clipbox);
    state->mask_enable = glc_getmask(c);
    glc_getcolor(c, state->color);
    glc_getclearcolor(c, state->clearcolor);
    glc_getblendmode(c, &state->blendmode);
}

static void apply_state(GLC_State *c, GLC_Config *state) {
    glc_useclipbox(c, state->clipbox_enable);
    if (state->clipbox_enable)
        glc_setclipbox(c, state->clipbox);
    glc_setcolor(c, state->color);
    glc_setclearcolor(c, state->clearcolor);
    glc_setblendmode(c, state->blendmode);
}

void glc_resetstate(GLC_State *c) {
    init_state(c->state);
    apply_state(c, c->state);
}

int glc_pushstate(GLC_State *c) {
    flush_state(c, c->state);
    push_context(c->state, GLC_Config);
    return 1;
}

void glc_popstate(GLC_State *c) {
    pop_context(c->state, GLC_Config);
    apply_state(c, c->state);
}

void glc_useclipbox(GLC_State *c, int enable) {
    if (enable)
        glEnable(GL_SCISSOR_TEST);
    else
        glDisable(GL_SCISSOR_TEST);
}

int glc_getclipbox(GLC_State *c, int rect[4]) {
    if (glIsEnabled(GL_SCISSOR_TEST) == GL_FALSE)
        return 0;
    glGetIntegerv(GL_SCISSOR_BOX, rect);
    return 1;
}

void glc_setclipbox(GLC_State *c, int rect[4]) {
    glEnable(GL_SCISSOR_TEST);
    /* Compensates for the fact that our y-coordinate is reverse of OpenGLs. */
    glScissor(rect[0], c->height - (rect[1] + rect[3]), rect[2], rect[3]);
}

void glc_setcolor(GLC_State *c, float color[4]) {
    glColor4fv(color);
}

void glc_getcolor(GLC_State *c, float color[4]) {
    glGetFloatv(GL_CURRENT_COLOR, color);
}

void glc_setclearcolor(GLC_State *c, float color[4]) {
    glClearColor(color[0], color[1], color[2], color[3]);
}

void glc_getclearcolor(GLC_State *c, float color[4]) {
    glGetFloatv(GL_COLOR_CLEAR_VALUE, color);
}

void glc_setblendmode(GLC_State *c, GLC_BlendMode blendmode) {
    switch (c->state->blendmode = blendmode) {
    case GLC_BLEND_SRC:
        glBlendFunc(GL_ONE, GL_ZERO);
        glDisable(GL_BLEND); break;

    case GLC_BLEND_SRC_IN:
        glBlendFunc(GL_DST_ALPHA, GL_ZERO);
        glEnable(GL_BLEND); break;

    case GLC_BLEND_DST_IN:
        glBlendFunc(GL_ZERO, GL_SRC_ALPHA);
        glEnable(GL_BLEND); break;

    case GLC_BLEND_SRC_OUT:
        glBlendFunc(GL_ONE_MINUS_DST_ALPHA, GL_ZERO);
        glEnable(GL_BLEND); break;

    case GLC_BLEND_DST_OUT:
        glBlendFunc(GL_ZERO, GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_BLEND); break;

    case GLC_BLEND_SRC_ATOP:
        glBlendFunc(GL_DST_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_BLEND); break;

    case GLC_BLEND_DST_ATOP:
        glBlendFunc(GL_ONE_MINUS_DST_ALPHA, GL_SRC_ALPHA);
        glEnable(GL_BLEND); break;

    case GLC_BLEND_DST_OVER:
        glBlendFunc(GL_ONE_MINUS_DST_ALPHA, GL_DST_ALPHA);
        glEnable(GL_BLEND); break;

    case GLC_BLEND_SRC_OVER: default:
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_BLEND); break;
    };
}

void glc_getblendmode(GLC_State *c, GLC_BlendMode *blendmode) {
    *blendmode = c->state->blendmode;
}


/* matrix routines */

static void apply_matrix(GLC_State *c) {
    float glmat[16], *m = c->matrix->mat;
    glmat[0] = m[0]; glmat[4] = m[3]; glmat[ 8] = 0.0f; glmat[12] = m[6];
    glmat[1] = m[1]; glmat[5] = m[4]; glmat[ 9] = 0.0f; glmat[13] = m[7];
    glmat[2] = 0.0f; glmat[6] = 0.0f; glmat[10] = 0.0f; glmat[14] = 0.0f;
    glmat[3] = m[2]; glmat[7] = m[5]; glmat[11] = 0.0f; glmat[15] = m[8];
    glLoadMatrixf(glmat);
}

int glc_pushmatrix(GLC_State *c) {
    push_context(c->matrix, GLC_Matrix);
    return 1;
}

void glc_popmatrix(GLC_State *c) {
    pop_context(c->matrix, GLC_Matrix);
    apply_matrix(c);
}

void glc_resetmatrix(GLC_State *c) {
    glc_matrix_load_identity(c->matrix->mat);
    glLoadIdentity();
}

void glc_setmatrix(GLC_State *c, float mat[9]) {
    glc_matrix_load_matrix(c->matrix->mat, mat);
    apply_matrix(c);
}

void glc_getmatrix(GLC_State *c, float mat[9]) {
    glc_matrix_load_matrix(mat, c->matrix->mat);
}

void glc_translate(GLC_State *c, float dx, float dy) {
    glc_matrix_translate(c->matrix->mat, dx, dy);
    apply_matrix(c);
}

void glc_rotate(GLC_State *c, float angle) {
    glc_matrix_rotate(c->matrix->mat, angle);
    apply_matrix(c);
}

void glc_scale(GLC_State *c, float sx, float sy) {
    glc_matrix_scale(c->matrix->mat, sx, sy);
    apply_matrix(c);
}

void glc_shear(GLC_State *c, float kx, float ky) {
    glc_matrix_shear(c->matrix->mat, kx, ky);
    apply_matrix(c);
}


/* canvas primitives fill */

static void fill_elements(GLenum type, size_t pointcount, float *pos) {
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(2, GL_FLOAT, 0, (const GLvoid*)pos);
    glDrawArrays(type, 0, pointcount);
    glDisableClientState(GL_VERTEX_ARRAY);
}

void glc_triangles(GLC_State *c, size_t count, float *pos) {
    fill_elements(GL_TRIANGLES, count, pos);
}

void glc_triangle_strip(GLC_State *c, size_t count, float *pos) {
    fill_elements(GL_TRIANGLE_STRIP, count, pos);
}

void glc_triangle_fan(GLC_State *c, size_t count, float *pos) {
    fill_elements(GL_TRIANGLE_FAN, count, pos);
}

void glc_rect(GLC_State *c, float pos[4]) {
    enum { X, Y, W, H };
    float quad[] = {
        pos[X],pos[Y],               pos[X],pos[Y]+pos[H],
        pos[X]+pos[W],pos[Y]+pos[H], pos[X]+pos[W],pos[Y],
    };
    fill_elements(GL_TRIANGLE_STRIP, 4, quad);
}


/* mask routines */

#define STENCIL_BIT     0x1
#define MASK_BIT        0x80

void glc_usemask(GLC_State *c, int enable) {
    if (enable) {
        glEnable(GL_STENCIL_TEST);
        glStencilFunc(GL_NOTEQUAL, MASK_BIT, MASK_BIT);
        glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
    }
    else
        glDisable(GL_STENCIL_TEST);
}

void glc_clearmask(GLC_State *c) {
    glClear(GL_STENCIL_BUFFER_BIT);
}

void glc_mask_polygon(GLC_State *c, size_t count, float *pos) {
    enum { MIN_X, MIN_Y, MAX_X, MAX_Y };
    int stencil_enable = glIsEnabled(GL_STENCIL_TEST);
    float bounds[4], *p;
    size_t i;

    for (i = 0, p = pos; i < count; ++i, p += 2) {
             if (bounds[MIN_X] > p[0]) bounds[MIN_X] = p[0];
        else if (bounds[MAX_X] < p[0]) bounds[MAX_X] = p[0];
             if (bounds[MIN_Y] > p[1]) bounds[MIN_Y] = p[1];
        else if (bounds[MAX_Y] < p[1]) bounds[MAX_Y] = p[1];
    }

    if (bounds[MIN_X] < 0        ) bounds[MIN_X] = 0;
    if (bounds[MAX_X] > c->width ) bounds[MAX_X] = c->width;
    if (bounds[MIN_Y] < 0        ) bounds[MIN_Y] = 0;
    if (bounds[MAX_Y] > c->height) bounds[MAX_Y] = c->height;

    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(2, GL_FLOAT, 0, pos);

    /* tesselation polygons into stencil buffer */
    if (!stencil_enable) glEnable(GL_STENCIL_TEST);

    glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);

    glStencilMask(STENCIL_BIT);
    glStencilFunc(GL_NOTEQUAL, MASK_BIT, MASK_BIT);
    glStencilOp(GL_ZERO, GL_INVERT, GL_INVERT);
    glDrawArrays(GL_TRIANGLE_FAN, 0, count);

    /* stencil to mask */
    glStencilMask(MASK_BIT);
    switch (c->maskmode) {
    case GLC_MASK_XOR:
        glStencilFunc(GL_EQUAL, 0xFF, STENCIL_BIT);
        glStencilOp(GL_KEEP, GL_INVERT, GL_INVERT);
        break;

    case GLC_MASK_UNION:
        glStencilFunc(GL_NOTEQUAL, 0xFF, STENCIL_BIT);
        glStencilOp(GL_KEEP, GL_REPLACE, GL_REPLACE);
        break;

    case GLC_MASK_INTERSECT:
        glStencilFunc(GL_EQUAL, STENCIL_BIT, STENCIL_BIT);
        glStencilOp(GL_ZERO, GL_KEEP, GL_KEEP);
        bounds[0] = 0;     bounds[1] = 0;
        bounds[2] = width; bounds[3] = height;
        break;

    case GLC_MASK_SUBTRACT:
        glStencilFunc(GL_EQUAL, STENCIL_BIT, STENCIL_BIT);
        glStencilOp(GL_KEEP, GL_ZERO, GL_ZERO);
        break;
    }

    glc_rect(c, bounds);

    glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
}

