#ifndef glc_matrix_h
#define glc_matrix_h


#include <math.h>


#ifndef GLC_INLINE
# define GLC_INLINE inline static
#endif /* GLC_INLINE */


/* vector 2 support */

GLC_INLINE float *vec2_init(float v[2], float x, float y)
{ v[0]=x,v[1]=y; return v; }

GLC_INLINE int vec2_equal(float u[2], float v[2])
{ return u[0]==v[0] && u[1]==v[1]; }

#define vec2_op(name, code) \
    float *vec2_##name(float u[2], const float v[2]) \
    { code; return u; }
GLC_INLINE vec2_op(set, ( u[0] =v[0], u[1] =v[1] ))
GLC_INLINE vec2_op(add, ( u[0]+=v[0], u[1]+=v[1] ))
GLC_INLINE vec2_op(sub, ( u[0]-=v[0], u[1]-=v[1] ))

GLC_INLINE float *vec2_neg(float v[2])
{ v[0]=-v[0], v[1]=-v[1]; return v; }

GLC_INLINE float *vec2_mul(float v[2], float n)
{ v[0]*=n, v[1]*=n; return v; }

GLC_INLINE float vec2_dot(const float u[2], const float v[2])
{ return v[0]*u[0] + u[1]*v[1]; }

GLC_INLINE float vec2_cross(const float u[2], const float v[2])
{ return u[0]*v[1] - v[0]*u[1]; }

GLC_INLINE float vec2_angle(const float v[2]) { 
    float a = atan2(v[1], v[0]);
    if (a < 0) a += 2*M_PI;
    return a;
}

GLC_INLINE float vec2_distance(float v[2])
{ return sqrt(v[0]*v[0] + v[1]*v[1]); }

GLC_INLINE float vec2_normalize(float v[2]) {
    float d = vec2_distance(v);
    if (d != 0) v[0] /= d, v[1] /= d;
    return d;
}


/* 
 * Matrix layout:
 * | 0 3 6 |    | sx kx tx |
 * | 1 4 7 | == | ky sy ty |
 * | 2 5 8 |    | w0 w1 w2 |
 */

void glc_matrix_load_identity  (float m[9]);
void glc_matrix_load_matrix    (float m[9], const float a[9]);
void glc_matrix_load_multiply  (float m[9], const float a[9], const float b[9]);
void glc_matrix_load_translate (float m[9], float x, float y);
void glc_matrix_load_rotate    (float m[9], float angle);
void glc_matrix_load_scale     (float m[9], float sx, float sy);
void glc_matrix_load_shear     (float m[9], float kx, float ky);

int glc_matrix_load_invert         (float m[9], const float inv[9]);
int glc_matrix_load_quad_to_square (float m[9], const float quad[8]);
int glc_matrix_load_square_to_quad (float m[9], const float quad[8]);
int glc_matrix_load_quad_to_quad   (float m[9], const float from[8], const float to[8]);

int  glc_matrix_invert    (float m[9]);
void glc_matrix_multiply  (float m[9], const float left[9]);
void glc_matrix_translate (float m[9], float x, float y);
void glc_matrix_rotate    (float m[9], float angle);
void glc_matrix_scale     (float m[9], float sx, float sy);
void glc_matrix_shear     (float m[9], float kx, float ky);

void glc_matrix_transform         (const float m[9], size_t pointcount, float *data);
void glc_matrix_transform_affline (const float m[9], size_t pointcount, float *data);
int  glc_matrix_transform_invert  (const float m[9], size_t pointcount, float *data);
int  glc_matrix_transform_invert_affline (const float m[9], size_t pointcount, float *data);


#endif /* glc_matrix_h */
