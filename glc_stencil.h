#ifndef glc_mask_h
#define glc_mask_h


#include <stddef.h>


typedef enum GLC_DrawElement {
    GLC_DRAW_TRIANGLES = 1,
    GLC_DRAW_TRIANGLE_FAN,
    GLC_DRAW_TRIANGLE_STRIP,
    GLC_DRAW_NUM_ELEMENTS
} GLC_DrawElement;

typedef enum GLC_MaskOp {
    GLC_MASK_SET = 1,
    GLC_MASK_UNION,
    GLC_MASK_INTERSECT,
    GLC_MASK_SUBTRACT,
    GLC_MASK_XOR,
    GLC_MASK_NUM_ENUM
} GLC_MaskOp;


void glc_stencil_clear(void);
void glc_stencil_draw(GLC_DrawElement e, size_t count, const float *pos);
void glc_stencil_polygon(size_t count, const float *pos);
void glc_stencil_calcbounds(float bounds[4], GLC_MaskOp op, size_t count, const float *pos);
void glc_stencil_fillbounds(float bounds[4]);
void glc_stencil_tomask(GLC_MaskOp op, const float bounds[4]);


#endif /* glc_mask_h */
