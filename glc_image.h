#ifndef glc_image_h
#define glc_image_h


#include <stddef.h>

struct GLCanvas;

typedef struct GLC_ImageObject *GLC_Image;

GLC_Image glc_image_new(int width, int height);
GLC_Image glc_image_from_file(const char *filename);
GLC_Image glc_image_from_data(const void *p, size_t len);
void      glc_image_delete(GLC_Image image);

int glc_image_width  (GLC_Image image);
int glc_image_height (GLC_Image image);

const void *glc_image_data(GLC_Image image);
void *glc_image_lock(GLC_Image image);
void  glc_image_unlock(GLC_Image image);

int  glc_pattern_image(struct GLCanvas *c, GLC_Image image);
void glc_drawimage(struct GLCanvas *c, GLC_Image image);


#endif /* glc_image_h */
