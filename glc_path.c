#include "glc_path.h"
#include "glc_matrix.h"
#include "glc_stencil.h"


#include <setjmp.h>
#include <math.h>


/* declare arrays */

typedef struct GLC_Contour {
    size_t count;
    int loop;
} GLC_Contour;

#define ARRAY_USERDATA     jmp_buf **pctx;
#define ARRAY_INIT(VEC)    vec->pctx = NULL;
#define ARRAY_NOMEM(vec)   if (vec->pctx) longjmp(**vec->pctx, 1);

#define ARRAY_PREFIX segs
#define ARRAY_TYPE   SegArray
#define ARRAY_ITEM   unsigned char
#include "array_tpl.h"

#define ARRAY_PREFIX contour
#define ARRAY_TYPE   ContourArray
#define ARRAY_ITEM   GLC_Contour
#include "array_tpl.h"

#define ARRAY_PREFIX vert
#define ARRAY_TYPE   VertexArray
#define ARRAY_ITEM   float
#include "array_tpl.h"

struct GLC_PathObject {
    jmp_buf *ctx; /* must be first */

    /* data */
    SegArray    segs;
    VertexArray data;

    /* caches */
    ContourArray contours; /* vertex contours info */
    VertexArray vertexs; /* vertex cache */
    VertexArray strokes; /* stroke vertex cache */

    /* stroke settings */
    float linewidth;
    float miterlimit;
    GLC_CapStyle capstyle;
    GLC_JoinStyle joinstyle;
};


/* path creations */

static void init_path(GLC_Path path) {
    segs_init(&path->segs);
    vert_init(&path->data);
    contour_init(&path->contours);
    vert_init(&path->vertexs);
    vert_init(&path->strokes);
    path->segs.pctx = &path->ctx;
    path->data.pctx = &path->ctx;
    path->contours.pctx = &path->ctx;
    path->vertexs.pctx = &path->ctx;
    path->strokes.pctx = &path->ctx;

    /* path stroke default settings */
    path->linewidth = 1.0f;
    path->miterlimit = 4.0f;
    path->capstyle = GLC_CAP_BUTT;
    path->joinstyle = GLC_JOIN_MITER;
}

GLC_Path glc_path_new() {
    GLC_Path path = (GLC_Path)malloc(sizeof(GLC_Path));
    if (path == NULL) return NULL;
    init_path(path);
    return path;
}

void glc_path_delete(GLC_Path path) {
    segs_cleanup(&path->segs);
    vert_cleanup(&path->data);
    contour_cleanup(&path->contours);
    vert_cleanup(&path->vertexs);
    vert_cleanup(&path->strokes);
    free(path);
}


/* path state maintains */

void glc_path_setlinewidth(GLC_Path path, float width) {
    path->linewidth = width;
}

void glc_path_setcapstyle(GLC_Path path, GLC_CapStyle style) {
    path->capstyle = style;
}

void glc_path_setjoinstyle(GLC_Path path, GLC_JoinStyle style) {
    path->joinstyle = style;
}

void glc_path_setmiterlimit(GLC_Path path, float limit) {
    path->miterlimit = limit;
}


/* path modifications */

void glc_path_clear(GLC_Path path) {
    segs_reset(&path->segs);
    vert_reset(&path->data);
    vert_reset(&path->contours);
    vert_reset(&path->vertexs);
    vert_reset(&path->strokes);
}

int glc_path_concat(GLC_Path dst, GLC_Path src) {
    return glc_path_append(dst,
            src->segs.size,
            src->segs.data,
            src->data.data);
}

int glc_path_append(GLC_Path dst, size_t count, const GLC_PathCmd *cmd, const float *data) {
    size_t orig_segs = dst->segs.size;
    size_t orig_data = dst->data.size;
    jmp_buf jbuf;

    dst->ctx = &jbuf;
    if (setjmp(jbuf) != 0) {
        dst->segs.size = orig_segs;
        dst->data.size = orig_data;
        return 0;
    }

    segs_append(&dst->segs, count, cmd);
    vert_append(&dst->data, count, data);
    return 1;
}


/* process path data interface */

#define GLC_SIMPLIFY           (1 << 0)
#define GLC_REPAIR_ENDS        (1 << 1)

typedef void PathDataVisitor(GLC_Path path, GLC_PathCmd cmd, float *data, void *ud);
typedef void ContourVisitor(GLC_Path path, float *v, size_t count, int loop, void *ud);

static void iter_path(GLC_Path path, int flags, PathDataVisitor *cb, void *ud);
static void iter_contour(GLC_Path path, ContourVisitor *cb, void *ud);


/* transform path */

static void update_bounds(GLC_Path path, float *data, size_t count, int loop, void *ud) {
    enum {X1, Y1, X2, Y2, X = X1, Y = Y1};
    float *bounds = (float*)ud;
    (void)path;
    for (; count-- > 0; data += 2) {
        if (bounds[X1] > data[X]) bounds[X1] = data[X];
        if (bounds[X2] < data[X]) bounds[X2] = data[X];
        if (bounds[Y1] > data[Y]) bounds[Y1] = data[Y];
        if (bounds[Y2] < data[Y]) bounds[Y2] = data[Y];
    }
}

void glc_path_bounds(GLC_Path path, float box[4]) {
    box[0] = box[1] = box[2] = box[3] = 0;
    iter_contour(path, update_bounds, (void*)box);
}

static void transform_vertex(GLC_Path path, float *data, size_t count, int loop, void *ud) {
    (void)path;
    glc_matrix_transform((float*)ud, count + loop, data);
}

void glc_path_transform(GLC_Path path, float m[9]) {
    iter_contour(path, transform_vertex, (void*)m);
}

void glc_path_interpolate(GLC_Path dst, GLC_Path start, GLC_Path stop) {
    /* XXX */
}


/* stencil path */

static int generate_vertex(GLC_Path path);
static int generate_stroke(GLC_Path path);

int glc_stencil_path(GLC_Path path) {
    if (path->vertexs.size == 0) {
        if (!generate_vertex(path))
            return 0;
    }
    glc_stencil_polygon(path->vertexs.size, path->vertexs.data);
    return 1;
}

int glc_stencil_stroke(GLC_Path path) {
    if (path->strokes.size == 0) {
        if (!generate_stroke(path))
            return 0;
    }
    glc_stencil_draw(GLC_DRAW_TRIANGLES, path->strokes.size, path->strokes.data);
    return 1;

}


/* parse path command string */

const char *skip_delimiter(const char *s) {
    const char *map = 
        "         tnklr                  "
        "s          ,                    "
        "                                "
        "                                ";
    while (map[*s&0x7F] != ' ')
        ++s;
    return s;
}

const char *parse_integer(const char *s, float *f) {
    unsigned ch;
    float n = 0;
    for (; (ch = *s) >= '0' && ch <= '9'; ++s) {
        n *= 10;
        n += (ch - '0');
    }
    if (f) *f = n;
    return s;
}

const char *parse_number(const char *s, float *n) {
    unsigned ch;
    float f = 0, l = 1;
    s = parse_integer(s, n);
    if (*s != '.') return s;
    while ((ch = *++s) >= '0' && ch <= '9') {
        l = l/10;
        f += l*(ch - '0');
    }
    if (n) *n += f;
    return s;
}

int parse_numbers(const char **ps, size_t count, float *ns) {
    const char *s = *ps;
    for (; count > 0; --count, ++ns) {
        const char *end = parse_number(skip_delimiter(s), ns);
        if (s == end) break; /* read nothing */
        s = end;
    }
    return count == 0;
}

const char *add_command(GLC_Path path, const char *s,
        GLC_PathCmd cmd, size_t count) {
    float buff[8];
    while (parse_numbers(&s, count, buff)) {
        *segs_push(&path->segs) = cmd;
        vert_append(&path->data, count, buff);
    }
    return s;
}

GLC_Path glc_path_from_string(const char *cmds) {
    GLC_Path path = glc_path_new();
    glc_path_appendstring(path, cmds);
    return path;
}

int glc_path_appendstring(GLC_Path path, const char *cmds) {
    jmp_buf jbuf;
    size_t orig_segs = path->segs.size;
    size_t orig_data = path->data.size;
    path->ctx = &jbuf;
    if (setjmp(jbuf) != 0) {
        path->segs.size = orig_segs;
        path->data.size = orig_data;
        return 0;
    }
    while (*cmds != '\0') {
        int isrel = 0;
        float buff[8];
        switch (*(cmds = skip_delimiter(cmds))) {
        case 'z': case 'Z': /* (none) */
            *segs_push(&path->segs) = GLC_CLOSE_PATH;
            break;
        case 'm': isrel = 1; case 'M': /* (x y)+ */
            cmds = add_command(path, cmds, GLC_MOVE_TO+isrel, 2);
            break;
        case 'l': isrel = 1; case 'L': /* (x y)+ */
            cmds = add_command(path, cmds, GLC_LINE_TO+isrel, 2);
            break;
        case 'h': isrel = 1; case 'H': /* x+ */
            cmds = add_command(path, cmds, GLC_HLINE_TO+isrel, 1);
            break;
        case 'v': isrel = 1; case 'V': /* y+ */
            cmds = add_command(path, cmds, GLC_HLINE_TO+isrel, 1);
            break;
        case 'c': isrel = 1; case 'C': /* (x1 y1 x2 y2 x y)+ */
            cmds = add_command(path, cmds, GLC_CUBIC_TO+isrel, 6);
            break;
        case 's': isrel = 1; case 'S': /* (x2 y2 x y)+ */
            cmds = add_command(path, cmds, GLC_SCUBIC_TO+isrel, 4);
            break;
        case 'q': isrel = 1; case 'Q': /* (x1 y1 x y)+ */
            cmds = add_command(path, cmds, GLC_QUAD_TO+isrel, 4);
            break;
        case 't': isrel = 1; case 'T': /* (x y)+ */
            cmds = add_command(path, cmds, GLC_SQUAD_TO+isrel, 2);
            break;
        case 'a': isrel = 1; case 'A': /* (rx ry rot large sweep x y)+ */
            while (parse_numbers(&cmds, 7, buff)) {
                GLC_PathCmd seg;
                if (buff[3] != 0)
                    seg = buff[4] != 0 ? GLC_LCCWARC_TO : GLC_LCWARC_TO;
                else
                    seg = buff[4] != 0 ? GLC_SCCWARC_TO : GLC_SCWARC_TO;
                *segs_push(&path->segs) = seg + isrel;
                vert_append(&path->data, 3, buff);
                vert_append(&path->data, 2, buff+5);
            }
            break;
        }
    }
    return 1;
}


/* process path data implement */

const size_t cmd_nargs[] = {
    0, /* GLC_CLOSE_PATH */
    2, /* GLC_MOVE_TO */
    2, /* GLC_LINE_TO */
    1, /* GLC_HLINE_TO */
    1, /* GLC_VLINE_TO */
    4, /* GLC_QUAD_TO */
    6, /* GLC_CUBIC_TO */
    2, /* GLC_SQUAD_TO */
    4, /* GLC_SCUBIC_TO */
    5, /* GLC_SCCWARC_TO */
    5, /* GLC_SCWARC_TO */
    5, /* GLC_LCCWARC_TO */
    5, /* GLC_LCWARC_TO */
};

static void iter_path(GLC_Path path, int flags, PathDataVisitor *cb, void *ud) {
    unsigned char *segs = path->segs.data;
    const float *data = path->data.data; /* path vertex data */
    float  buff[16]; /* path command args passed to cb */
    float  start[2];        /* start of current contour */
    float  tan[2];          /* backward tangent for smmoothing */
    float *pen  = buff;     /* current pen position */
    float *args = buff + 2; /* extra arguments passed to cb  */
    int i, open = 0;        /* contour-open flag */

    /* reset  */
    vec2_init(start, 0, 0);
    vec2_init(tan,   0, 0);
    vec2_init(pen,   0, 0);

    for (i = 0; i < path->segs.size; ++i) {
        enum { X, Y };
        int cmd = *segs++; /* current path command */
        int realcmd = cmd&~1;
        int is_rel  = ((cmd&1) == GLC_PATH_RELATIVE);

        if (flags & GLC_REPAIR_ENDS) {
            /* prevent double CLOSE_PATH */
            if (!open && realcmd == GLC_CLOSE_PATH)
                continue;

            /* implicit MOVE_TO if segment start without */
            if (!open && realcmd != GLC_MOVE_TO) {
                vec2_set(&args[0], pen);
                cb(path, GLC_MOVE_TO, buff, ud);
                open = 1;
            }

            if (realcmd == GLC_MOVE_TO) {
                /* avoid a MOVE_TO at the end of data */
                if (i == path->segs.size - 1)
                    break;

                /* avoid a lone MOVE_TO  */
                if ((*segs&~1) == GLC_MOVE_TO) {
                    open = 0;
                    continue;
                }

                /* add extra close-path to loop contour */
                if (i != 0 && vec2_equal(pen, start)) {
                    vec2_set(&args[0], start);
                    cb(path, GLC_CLOSE_PATH, buff, ud);
                }
            }
        }

        /* ignore none commands */
        if (cmd < 0 || cmd >= GLC_PATH_NUM_COMMANDS)
            continue;
        /* copy data to args */
        {
            int i, nargs;
            nargs = cmd_nargs[cmd>>1];
            for (i = 0; i < nargs; ++i)
                args[i] = *data++;
        }

        /* process args */
        switch (realcmd) {
        case GLC_CLOSE_PATH:
            open = 0;

            vec2_set(args,
                vec2_set(pen,
                vec2_set(tan, start)));
            cb(path, GLC_CLOSE_PATH, buff, ud);
            break;

        case GLC_MOVE_TO:
            open = 1;

            if (is_rel) vec2_add(args, pen);
            vec2_set(start,
                vec2_set(pen,
                vec2_set(tan, args)));
            cb(path, GLC_MOVE_TO, buff, ud);
            break;

        case GLC_LINE_TO:
            if (is_rel) vec2_add(args, pen);

            vec2_set(pen, args);
            vec2_set(tan, pen);
            cb(path, GLC_LINE_TO, buff, ud);
            break;

        case GLC_HLINE_TO:
            if (is_rel) args[0] += pen[X];

            pen[X] = args[0];
            vec2_set(tan, pen);

            if (flags & GLC_SIMPLIFY) {
                args[1] = pen[Y];
                cb(path, GLC_LINE_TO, buff, ud);
                break;
            }
            cb(path, GLC_HLINE_TO, buff, ud);
            break;

        case GLC_VLINE_TO:
            if (is_rel) args[0] += pen[Y];

            pen[Y] = args[Y];
            vec2_set(tan, pen);

            if (flags & GLC_SIMPLIFY) {
                vec2_set(args, pen);
                cb(path, GLC_LINE_TO, buff, ud);
                break;
            }
            cb(path, GLC_VLINE_TO, buff, ud);
            break;

        case GLC_QUAD_TO:
            if (is_rel) {
                vec2_add(&args[0], pen);
                vec2_add(&args[2], pen);
            }

            vec2_set(tan, &args[0]);
            vec2_set(pen, &args[2]);
            cb(path, GLC_QUAD_TO, buff, ud);
            break;

        case GLC_CUBIC_TO:
            if (is_rel) {
                vec2_add(&args[0], pen);
                vec2_add(&args[2], pen);
                vec2_add(&args[4], pen);
            }

            vec2_set(tan, &args[2]);
            vec2_set(pen, &args[4]);
            cb(path, GLC_CUBIC_TO, buff, ud);
            break;

        case GLC_SQUAD_TO:
            if (is_rel) vec2_add(&args[0], pen);

            vec2_init(tan, 2*pen[X] - tan[X], 2*pen[Y] - tan[Y]);
            vec2_set(pen, &args[0]);

            if (flags & GLC_SIMPLIFY) {
                vec2_set(&args[0], tan);
                vec2_set(&args[2], pen);
                cb(path, GLC_QUAD_TO, buff, ud);
                break;
            }
            cb(path, GLC_SQUAD_TO, buff, ud);
            break;

        case GLC_SCUBIC_TO:
            if (is_rel) {
                vec2_add(&args[0], pen);
                vec2_add(&args[2], pen);
            }

            vec2_set(tan, &args[0]);
            vec2_set(pen, &args[1]);

            if (flags & GLC_SIMPLIFY) {
                vec2_init(&args[0], 2*pen[X] - tan[X], 2*pen[Y] - tan[Y]);
                vec2_set(&args[2], tan);
                vec2_set(&args[4], pen);
                cb(path, GLC_CUBIC_TO, buff, ud);
                break;
            }
            cb(path, GLC_SCUBIC_TO, buff, ud);

        case GLC_SCCWARC_TO:
        case GLC_SCWARC_TO:
        case GLC_LCCWARC_TO:
        case GLC_LCWARC_TO:
            if (is_rel) vec2_add(&args[3], pen);
            if (args[0] < 0) args[0] = -args[0];
            if (args[1] < 0) args[1] = -args[1];
            vec2_set(tan, &args[3]);
            vec2_set(pen, tan);
            cb(path, realcmd, buff, ud);
            break;
        }
    }
}


/* vertex addition interface */

static size_t add_vertex(VertexArray *v, float *data); /* X, Y */
static size_t add_curve3(VertexArray *v, float *data); /* X1,Y1, X2,Y2, X3,Y3 */
static size_t add_curve4(VertexArray *v, float *data); /* X1,Y1, X2,Y2, X3,Y3, X4,Y4 */
static size_t add_arc(VertexArray *v, float *data);    /* CX,CY, RX,RY, ANGLE, START,STOP */

static size_t add_triangle(VertexArray *v, float *data); /* X1,Y1, X2,Y2, X3,Y3 */
static size_t add_quad(VertexArray *v, float *data);     /* X1,Y1, X2,Y2, X3,Y3, X4,Y4 */
static size_t add_pie(VertexArray *v, float *data, size_t step); /* CX,CY, R, START,STOP */


/* generate path vertex */

typedef struct GeneratorCtx {
    VertexArray *v;
    ContourArray *c;
    GLC_PathCmd cmd;
} GeneratorCtx;

static int centralize_arc(GLC_PathCmd cmd, const float *data, float *outargs) {
    int is_large_arc = (cmd==GLC_LCWARC_TO || cmd==GLC_LCCWARC_TO);
    int is_ccw_arc   = (cmd==GLC_LCWARC_TO || cmd==GLC_SCWARC_TO);
    enum { X0, Y0, RX, RY, ANGLE, X2, Y2, X = X0, Y = Y0 };
    float rx = data[RX], ry = data[RY];
    float x1, y1, cx, cy, cx1, cy1;
    float cos_a, sin_a;
    float start_angle, sweep_angle;
    int m_radii_ok = 0;
    
    /* Calculate the middle point between 
     * the current and the final points
     */
    {
        float dx2 = (data[X0] - data[X2]) / 2.0;
        float dy2 = (data[Y0] - data[Y2]) / 2.0;

        cos_a = cos(data[ANGLE]);
        sin_a = sin(data[ANGLE]);

        /* Calculate (x1, y1) */
        x1 =  cos_a * dx2 + sin_a * dy2;
        y1 = -sin_a * dx2 + cos_a * dy2;
    }

    /* Ensure radii are large enough */
    {
        float sign, sq, coef;
        float radii_check;
        float prx = rx * rx;
        float pry = ry * ry;
        float px1 = x1 * x1;
        float py1 = y1 * y1;

        /* Check that radii are large enough */
        radii_check = px1/prx + py1/pry;
        if(radii_check > 1.0) {
            rx = sqrt(radii_check) * rx;
            ry = sqrt(radii_check) * ry;
            prx = rx * rx;
            pry = ry * ry;
            if(radii_check > 10.0) m_radii_ok = 0;
        }

        /* Calculate (cx1, cy1) */
        sign = (is_large_arc == is_ccw_arc) ? -1.0 : 1.0;
        sq   = (prx*pry - prx*py1 - pry*px1) / (prx*py1 + pry*px1);
        coef = sign * sqrt((sq < 0) ? 0 : sq);
        cx1 = coef *  ((rx * y1) / ry);
        cy1 = coef * -((ry * x1) / rx);
    }

    /* Calculate (cx, cy) from (cx1, cy1) */
    {
        float sx2 = (data[X0] + data[X2]) / 2.0;
        float sy2 = (data[Y0] + data[Y2]) / 2.0;
        cx = sx2 + (cos_a * cx1 - sin_a * cy1);
        cy = sy2 + (sin_a * cx1 + cos_a * cy1);
    }

    /* Calculate the start_angle (angle1)
     * and the sweep_angle (dangle) */
    {
        float ux =  (x1 - cx1) / rx;
        float uy =  (y1 - cy1) / ry;
        float vx = (-x1 - cx1) / rx;
        float vy = (-y1 - cy1) / ry;

        /* Calculate the angle start */
        float n = sqrt(ux*ux + uy*uy);
        float p = ux; /* (1 * ux) + (0 * uy) */
        float sign = (uy < 0) ? -1.0 : 1.0;
        float v = p / n;
        if(v < -1.0) v = -1.0;
        if(v >  1.0) v =  1.0;
        start_angle = sign * acos(v);

        /* Calculate the sweep angle */
        n = sqrt((ux*ux + uy*uy) * (vx*vx + vy*vy));
        p = ux*vx + uy*vy;
        sign = (ux*vy - uy*vx < 0) ? -1.0 : 1.0;
        v = p / n;
        if(v < -1.0) v = -1.0;
        if(v >  1.0) v =  1.0;
        sweep_angle = sign * acos(v);
        if(!is_ccw_arc && sweep_angle > 0) 
            sweep_angle -= M_PI * 2.0;
        else if (is_ccw_arc && sweep_angle < 0) 
            sweep_angle += M_PI * 2.0;
    }

    outargs[0] = cx;
    outargs[1] = cy;
    outargs[2] = rx;
    outargs[3] = ry;
    outargs[4] = data[ANGLE];
    outargs[5] = start_angle;
    outargs[6] = sweep_angle;
    return m_radii_ok;
}

static void vertex_generator(GLC_Path path, GLC_PathCmd cmd, float *data, void *ud) {
    GeneratorCtx *ctx = (void*)ud;
    float arcargs[12];
    GLC_Contour *contour = contour_last(ctx->c);
    VertexArray *v = ctx->v;
    switch (ctx->cmd = cmd) {
    case GLC_MOVE_TO:
        contour = contour_push(ctx->c);
        contour->count = 1;
        contour->loop = 0;
        add_vertex(v, data + 2);
        break;
    case GLC_CLOSE_PATH:
        contour->loop = 1;
        add_vertex(v, data + 2);
        break;
    case GLC_LINE_TO:
        ++contour->count;
        add_vertex(v, data + 2);
        break;
    case GLC_QUAD_TO:
        contour->count += add_curve3(v, data);
        break;
    case GLC_CUBIC_TO:
        contour->count += add_curve4(v, data);
        break;
    case GLC_SCWARC_TO:
    case GLC_SCCWARC_TO:
    case GLC_LCWARC_TO:
    case GLC_LCCWARC_TO:
        centralize_arc(cmd, data, arcargs);
        contour->count += add_arc(v, arcargs);
        break;
    }
}

static int generate_vertex(GLC_Path path) {
    jmp_buf jbuf;
    GeneratorCtx ctx;
    ctx.v     = &path->vertexs;
    ctx.c     = &path->contours;

    path->ctx = &jbuf;
    if (setjmp(jbuf) != 0) {
        vert_reset(&path->vertexs);
        contour_reset(&path->contours);
        return 0;
    }

    vert_reset(&path->vertexs);
    contour_reset(&path->contours);
    iter_path(path, GLC_SIMPLIFY|GLC_REPAIR_ENDS, vertex_generator, (void*)&ctx);
    return 1;
}

static void iter_contour(GLC_Path path, ContourVisitor *cb, void *ud) {
    if (path->vertexs.size == 0) generate_vertex(path);
    if (cb != NULL) {
        GLC_Contour *contours = path->contours.data;
        float *data = path->vertexs.data;
        size_t clen = path->contours.size;
        for (; clen-- > 0; ++contours) {
            cb(path, data, contours->count, contours->loop, ud);
            data += (contours->count + contours->loop) * 2;
        }
    }
}


/* vertex generator */

static size_t add_vertex(VertexArray *v, float *data) {
    vert_append(v, 2, data);
    return 1;
}

static size_t add_curve3(VertexArray *v, float *data) {
    enum { X1, Y1, X2, Y2, X3, Y3, X = X1, Y = Y1 };
#if 1 /* inc method */
    size_t res, num_steps;
    float subdiv_step, subdiv_step2;
    float tmp[2], f[2], df[2], ddf[2];

    {
        float d1[2], d2[2], len;
        vec2_set(d1, data + 2); vec2_sub(d1, data + 0);
        vec2_set(d2, data + 4); vec2_sub(d2, data + 2);
        len = (vec2_distance(d1) + vec2_distance(d2)) * 0.25/* * SCALE */;
        num_steps = (size_t)len;
    }

    if (num_steps < 4) num_steps = 4;

    subdiv_step  = 1.0f/num_steps;
    subdiv_step2 = subdiv_step*subdiv_step;

    tmp[X] = (data[X1] - data[X2] * 2 + data[X3]) * subdiv_step2;
    tmp[Y] = (data[Y1] - data[Y2] * 2 + data[Y3]) * subdiv_step2;

    vec2_set(f, data);
    df[X] = tmp[X] + (data[X2] - data[X1]) * 2*subdiv_step;
    df[Y] = tmp[Y] + (data[Y2] - data[Y1]) * 2*subdiv_step;
    ddf[X] = tmp[X] * 2;
    ddf[Y] = tmp[Y] * 2;

    /* generate */
    res = num_steps + 1;
    add_vertex(v, data); /* start point */
    while (--num_steps > 0) {
        vec2_add(f, df); vec2_add(df, ddf);
        add_vertex(v, f); /* cur point */
    }
    add_vertex(v, data + 4); /* end point */
    return res;
#else /* subdiv method */
#endif
}

static size_t add_curve4(VertexArray *v, float *data) {
    enum { X1, Y1, X2, Y2, X3, Y3, X4, Y4, X = X1, Y = Y1 };
#if 1 /* inc method */
    size_t res, num_steps;
    float subdiv_step, subdiv_step2, subdiv_step3;
    float pre1, pre2, pre4, pre5;
    float tmp1[2], tmp2[2];
    float f[2], df[2], ddf[2], dddf[2];

    {
        float d1[2], d2[2], d3[2], len;
        vec2_set(d1, data + 2); vec2_sub(d1, data + 0);
        vec2_set(d2, data + 4); vec2_sub(d2, data + 2);
        vec2_set(d3, data + 4); vec2_sub(d3, data + 2);
        len = vec2_distance(d1) + vec2_distance(d2) + vec2_distance(d3);
        num_steps = (size_t)(len * 0.25 /* *SCALE */);
    }

    if (num_steps < 4) num_steps = 4;

    subdiv_step  = 1.0f/num_steps;
    subdiv_step2 = subdiv_step*subdiv_step;
    subdiv_step3 = subdiv_step2*subdiv_step;

    pre1 = 3*subdiv_step;
    pre2 = 3*subdiv_step2;
    pre4 = 6*subdiv_step2;
    pre5 = 6*subdiv_step3;

    tmp1[X] = data[X1] - 2*data[X2] + data[X3];
    tmp1[Y] = data[Y1] - 2*data[Y2] + data[Y3];
    tmp2[X] = 3*(data[X2] - data[X3]) + data[X4];
    tmp2[Y] = 3*(data[Y2] - data[Y3]) + data[Y4];

    vec2_set(f, data);
    df[X] = pre1*(data[X2] - data[X1]) + pre2*tmp1[X] + subdiv_step3*tmp2[X];
    df[Y] = pre1*(data[Y2] - data[Y1]) + pre2*tmp1[Y] + subdiv_step3*tmp2[Y];
    ddf[X] = pre4*tmp1[X] + pre5*tmp2[X];
    ddf[Y] = pre4*tmp1[Y] + pre5*tmp2[Y];
    dddf[X] = pre5*tmp2[X];
    dddf[Y] = pre5*tmp2[Y];

    /* generate */
    res = num_steps + 1;
    add_vertex(v, data); /* start point */
    while (--num_steps > 0) {
        vec2_add(f, df);
        vec2_add(df, ddf);
        vec2_add(ddf, dddf);
        add_vertex(v, f); /* cur point */
    }
    add_vertex(v, data + 6); /* end point */
    return res;
#else /* subdiv method */
#endif
}

static size_t add_arc(VertexArray *v, float *data) {
    enum { CX, CY, RX, RY, ANGLE, START, STOP, X = CX, Y = CY };
    size_t res = 0;
    float m[9], vert[2], angle, da;
    glc_matrix_load_rotate(m, data[ANGLE]);
    glc_matrix_translate(m, data[CX], data[CY]);

    /* normlize arc */
    {
        float ra = (data[RX] + data[RY]) / 2;
        da = acos(ra / (ra + 0.125/* /SCALE */)) * 2;
    }

    for (angle = data[START]; angle < data[STOP]; angle += da) {
        vert[X] = cos(da) * data[RX],
        vert[Y] = sin(da) * data[RY], 
        glc_matrix_transform(m, 1, vert);
        add_vertex(v, vert);
        ++res;
    }
    vert[X] = cos(data[STOP]) * data[RX];
    vert[Y] = sin(data[STOP]) * data[RY];
    glc_matrix_transform(m, 1, vert);
    add_vertex(v, vert);
    return res + 1;
}

static size_t add_triangle(VertexArray *v, float *data) {
    vert_append(v, 6, data);
    return 3;
}

static size_t add_quad(VertexArray *v, float *data) {
    float vert[12];
    vec2_set(vert+0, data+0);
    vec2_set(vert+1, data+2);
    vec2_set(vert+2, data+4);
    vec2_set(vert+3, data+0);
    vec2_set(vert+4, data+4);
    vec2_set(vert+5, data+6);
    vert_append(v, 12, data);
    return 6;
}

static size_t add_pie(VertexArray *v, float D[5], size_t step) {
    enum { X,Y, R, START,STOP };
    size_t res = 0;
    float vert[6], da;
    if (step == 0) step = 12; /* default step */
    da = (D[STOP] - D[START]) / step;
    vec2_set(vert, D);
    vec2_init(vert+2,
            D[X] + D[R]*cos(D[START]),
            D[Y] + D[R]*sin(D[START]));
    while ((D[START] += da) < D[STOP]) {
        vec2_init(vert+4,
                D[X] + D[R]*cos(D[START]),
                D[Y] + D[R]*sin(D[START]));
        res += add_triangle(v, vert);
        vec2_set(vert+2, vert+4);
    }
    vec2_init(vert+4,
            D[X] + D[R]*cos(D[STOP]),
            D[Y] + D[R]*sin(D[STOP]));
    res += add_triangle(v, vert);
    return res;
}


/* dash generator */

typedef struct VCGen {
    GLC_Path path;
    ContourVisitor *cb;
    void *ud;
} VCGen;

typedef struct DashGenCtx {
    VCGen ctx;
    float *dashes;
    size_t num_dashes;
    float dashs_len;
    size_t curr_dash;
    float curr_dash_start;
    float curr_dash_rest;
    int v1_dist, v2_dist;
} DashGenCtx;

/*static void dash_generator(void *ud, float *data, size_t count, int loop);*/


/* stroke generator */

static int line_intersection(float cross[2], const float V[8]) {
    enum { X1,Y1, X2,Y2, X3,Y3, X4,Y4 };
    float r;
    float num = (V[Y1]-V[Y3]) * (V[X4]-V[X3]) - (V[X1]-V[X3]) * (V[Y4]-V[Y3]);
    float den = (V[X2]-V[X1]) * (V[Y4]-V[Y3]) - (V[Y2]-V[Y1]) * (V[X4]-V[X3]);
    if(fabs(den) < 0.f) return 0;
    r = num / den;
    cross[X1] = V[X1] + r * (V[X2]-V[X1]);
    cross[Y1] = V[Y1] + r * (V[Y2]-V[Y1]);
    return 1;
}

static void add_join(VertexArray *v, float *V, float *norms,
        GLC_JoinStyle style, float hw, float limit) {
    enum { X1, Y1, X2, Y2, X3, Y3, X=X1, Y=Y1 };
    float buff[8], t1[2], t2[2], length;
    float cross = vec2_cross(&norms[X1], &norms[X2]);
    vec2_set(t1, &norms[X1]), vec2_set(t2, &norms[X2]);
    if (cross < 0)
        vec2_neg(t1), vec2_neg(t2);
    switch (style) {
    default: break;
    case GLC_JOIN_MITER:
        /* calculate miter length */
        {
            float angle1 = vec2_angle(t1);
            float angle2 = vec2_angle(t2);
            length = 1/cos((angle2-angle1)/2);
        }
        if (length <= limit) {
            float lines[8];
                     vec2_set(buff+0, &V[X2]);
            vec2_add(vec2_set(buff+2, &V[X2]), t1);
            vec2_add(vec2_set(buff+6, &V[X2]), t2);
            vec2_add(vec2_set(lines+0, &V[X1]), t1);
                     vec2_set(lines+2, buff+2);
                     vec2_set(lines+4, buff+6);
            vec2_add(vec2_set(lines+6, &V[X3]), t2);
            if (line_intersection(buff+4, lines)) {
                add_quad(v, buff);
                break;
            }
            /* can not calc intersections
             * go though to bevel join.
             */
        }
    case GLC_JOIN_BEVEL:
        if (cross > 0) { /* ccw */
            vec2_init(buff+0, V[X2]+t1[X], V[Y2]+t1[Y]);
            vec2_init(buff+1, V[X2],       V[Y2]);
            vec2_init(buff+2, V[X2]+t2[X], V[Y2]+t2[Y]);
        }
        else { /* cw */
            vec2_init(buff+0, V[X2]+t1[X], V[Y2]+t1[Y]);
            vec2_init(buff+2, V[X2]+t2[X], V[Y2]+t2[Y]);
            vec2_init(buff+1, V[X2],       V[Y2]);
        }
        add_triangle(v, buff);
        break;
    case GLC_JOIN_ROUND:
        vec2_set(buff, &V[X2]); /* CX,CY */
        buff[2] = hw; /* R */
        buff[3] = vec2_angle(t1);
        buff[4] = vec2_angle(t2);
        add_pie(v, buff, 0);
        break;
    }
}

static void add_cap(VertexArray *v, float *vert, float *norms,
        GLC_CapStyle style, float hw, int start) {
    enum { X, Y };
    float t[2], buff[8];
    vec2_set(t, norms);
    if (start) vec2_mul(t, -1);
    switch (style) {
    default: break;
    case GLC_CAP_ROUND:
        vec2_set(buff, vert); /* CX,CY */
        buff[2] = hw;         /* R */
        buff[3] = vec2_angle(t); /* ANGLE */
        buff[4] = buff[3] + M_PI;
        add_pie(v, buff, 0);
        break;
    case GLC_CAP_SQUARE:
        buff[0+X] = vert[X]   +    t[X];
        buff[0+Y] = vert[Y]   +    t[Y];
        buff[1+X] = buff[0+X] +    t[Y];
        buff[1+Y] = buff[0+Y] +   -t[X];
        buff[2+X] = buff[1+X] + -2*t[X];
        buff[2+Y] = buff[1+Y] + -2*t[Y];
        buff[3+X] = buff[2+X] +   -t[Y];
        buff[3+Y] = buff[2+Y] +    t[X];
        add_quad(v, buff);
        break;
    }
}

static float calc_normale(float norms[2], const float V[4], float hw) {
    enum { X1,Y1, X2,Y2, X=X1,Y=Y1 };
    float dirvec[2], dist; /* direction vector */
    /* set current direction vector */
    dirvec[X] = V[X2]-V[X1];
    dirvec[Y] = V[Y2]-V[Y1];
    /* set current normal vector */
    norms[X] = -dirvec[Y];
    norms[Y] =  dirvec[X];
    dist = vec2_distance(norms);
    vec2_mul(norms, hw/dist);
    return dist;
}

static void stroke_generator(GLC_Path path, float *data, size_t count, int loop, void *ud) {
    enum { X, Y };
    VertexArray *v = (VertexArray*)ud;

    size_t i;
    float norms[4], hw;

    /* alone vertex is ignored */
    if (count <= 1) return;

    /* calc segment info of first vertex */
    hw = path->linewidth/2;
    calc_normale(norms, data, hw);

    /* if not loop line, add first line cap */
    if (!loop) {
        add_cap(v, data, norms, path->capstyle, hw, 1);
        --count; /* if not loop, do not iter last vertex */
    }

    /* walk though vertexs */
    for (i = 0; i < count; ++i, data += 2) {
        float buff[8];

        /* make line segment */
        buff[0+X] = data[0+X]+norms[X];
        buff[0+Y] = data[0+Y]+norms[Y];
        buff[1+X] = data[2+X]+norms[X];
        buff[1+Y] = data[2+Y]+norms[Y];
        buff[2+X] = data[2+X]-norms[X]*2;
        buff[2+Y] = data[2+Y]-norms[Y]*2;
        buff[3+X] = data[0+X]-norms[X]*2;
        buff[3+Y] = data[0+Y]-norms[Y]*2;
        add_quad(v, buff);

        /* calc segment info of next line */
        calc_normale(norms+2, data+2, hw);

        /* make line join */
        add_join(v, data, norms,
                path->joinstyle, hw,
                path->miterlimit);

        vec2_set(norms, norms+2);
    }

    /* add end line cap if not loop line */
    if (!loop) add_cap(v, data, norms, path->capstyle, hw, 0);
}

static int generate_stroke(GLC_Path path) {
    jmp_buf jbuf;

    if (path->vertexs.size == 0 && !generate_vertex(path))
        return 0;

    path->ctx = &jbuf;
    if (setjmp(jbuf) != 0) {
        vert_reset(&path->strokes);
        return 0;
    }

    /* XXX dash support NYI */
    vert_reset(&path->strokes);
    iter_contour(path, stroke_generator, NULL);
    return 1;
}

/* cc: flags+='-O3 -Werror -pedantic' */
