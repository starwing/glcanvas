#if !defined(ARRAY_TYPE) || !defined(ARRAY_ITEM) || !defined(ARRAY_PREFIX)
# error define ARRAY_TYPE, ARRAY_ITEM and ARRAY_PREFIX before include this header
#endif


#define CONCAT_(a, b) a ## b
#define CONCAT(a, b) CONCAT_(a, b)
#define FN(n) CONCAT(CONCAT(ARRAY_PREFIX, _), n)


#if !defined(ARRAY_DECLARE) && !defined(ARRAY_DEFINE)
#  define ARRAY_DECLARE
#  define ARRAY_DEFINE
#  define ARRAY_UNDEF_DECLARE_AND_DEFINE_
#endif

#ifndef   ARRAY_FUNC
#  define ARRAY_FUNC_DEFAULT_ /* none */
#  define ARRAY_FUNC ARRAY_FUNC_DEFAULT_
#endif /* ARRAY_FUNC */


#ifdef ARRAY_DECLARE

#include <stddef.h>

typedef struct ARRAY_TYPE {
    size_t capacity;
    size_t size;
    ARRAY_ITEM *data;
#ifdef ARRAY_USERDATA
    ARRAY_USERDATA
#endif
} ARRAY_TYPE;

ARRAY_FUNC void FN(init)    (ARRAY_TYPE *vec);
ARRAY_FUNC void FN(reset)   (ARRAY_TYPE *vec);
ARRAY_FUNC void FN(cleanup) (ARRAY_TYPE *vec);

ARRAY_FUNC ARRAY_ITEM *FN(reverse) (ARRAY_TYPE *vec, size_t size);
ARRAY_FUNC ARRAY_ITEM *FN(push)    (ARRAY_TYPE *vec);
ARRAY_FUNC ARRAY_ITEM *FN(pop)     (ARRAY_TYPE *vec);

ARRAY_FUNC int FN(append) (ARRAY_TYPE *vec, size_t count, const ARRAY_ITEM *data);
ARRAY_FUNC int FN(concat) (ARRAY_TYPE *dst, const ARRAY_TYPE *src);

ARRAY_FUNC ARRAY_ITEM *FN(first)  (ARRAY_TYPE *vec);
ARRAY_FUNC ARRAY_ITEM *FN(last)   (ARRAY_TYPE *vec);
ARRAY_FUNC ARRAY_ITEM *FN(index)  (ARRAY_TYPE *vec, int idx);
ARRAY_FUNC ARRAY_ITEM *FN(insert) (ARRAY_TYPE *vec, int idx, size_t n);
ARRAY_FUNC void        FN(remove) (ARRAY_TYPE *vec, int idx, size_t n);

#endif /* ARRAY_DECLARE */


#ifdef ARRAY_DEFINE

#include <stdlib.h>
#include <string.h>

ARRAY_FUNC void FN(init) (ARRAY_TYPE *vec) {
    vec->capacity = vec->size = 0;
    vec->data = NULL;
#ifdef ARRAY_INIT
    ARRAY_INIT(vec)
#endif /* ARRAY_INIT */
}

ARRAY_FUNC void FN(reset) (ARRAY_TYPE *vec) {
    vec->size = 0;
}

ARRAY_FUNC void FN(cleanup) (ARRAY_TYPE *vec) {
    free(vec->data);
    vec->capacity = vec->size = 0;
#ifdef ARRAY_DEINIT
    ARRAY_DEINIT(vec)
#endif /* ARRAY_DEINIT */
}

ARRAY_FUNC ARRAY_ITEM *FN(reverse) (ARRAY_TYPE *vec, size_t size) {
    size_t capacity = vec->size + size;
    if (capacity > vec->capacity) {
        ARRAY_ITEM *data;
        if (capacity < vec->capacity * 2)
            capacity = vec->capacity * 2;
        data = (ARRAY_ITEM*)realloc(vec->data, capacity);
        if (data == NULL) {
#ifdef ARRAY_NOMEM
            ARRAY_NOMEM(vec)
#endif /* ARRAY_NOMEM */
            return NULL;
        }
        vec->data = data;
        vec->capacity = capacity;
    }
    return vec->data + vec->size;
}

ARRAY_FUNC ARRAY_ITEM *FN(first) (ARRAY_TYPE *vec) {
    return vec->size == 0 ? NULL : vec->data;
}

ARRAY_FUNC ARRAY_ITEM *FN(last) (ARRAY_TYPE *vec) {
    return vec->size == 0 ? NULL : &vec->data[vec->size];
}

ARRAY_FUNC ARRAY_ITEM *FN(push) (ARRAY_TYPE *vec) {
    ARRAY_ITEM *data = FN(reverse)(vec, 1);
    if (data != NULL) ++vec->size;
    return data;
}

ARRAY_FUNC ARRAY_ITEM *FN(pop) (ARRAY_TYPE *vec) {
    return vec->size > 0 ?
          &vec->data[--vec->size] : NULL;
}

ARRAY_FUNC int FN(append) (ARRAY_TYPE *vec, size_t count, const ARRAY_ITEM *data) {
    if (count != 0) {
        ARRAY_ITEM *p = FN(reverse)(vec, count);
        if (p == NULL) return 0;
        memcpy(p, data, count * sizeof(ARRAY_ITEM));
        vec->size += count;
    }
    return 1;
}

ARRAY_FUNC int FN(concat) (ARRAY_TYPE *dst, const ARRAY_TYPE *src) {
    return FN(append)(dst, src->size, src->data);
}

ARRAY_FUNC ARRAY_ITEM *FN(index) (ARRAY_TYPE *vec, int idx) {
    if (idx < 0) idx += vec->size;
    if (idx < 0 || idx >= vec->size) return NULL;
    return &vec->data[idx];
}

ARRAY_FUNC ARRAY_ITEM *FN(insert) (ARRAY_TYPE *vec, int idx, size_t n) {
    if (idx < 0) idx += vec->size;
    if (idx < 0 || idx >= vec->size) return NULL;
    if (!FN(reverse)(vec, n)) return NULL;
    memmove(vec->data + idx + n,
            vec->data + idx,
            (vec->size - idx) * sizeof(ARRAY_ITEM));
    vec->size += n;
    return vec->data + idx;
}

ARRAY_FUNC void FN(remove) (ARRAY_TYPE *vec, int idx, size_t n) {
    if (idx < 0) idx += vec->size;
    if (idx < 0 || idx >= vec->size) return;
    if (idx + n > vec->size) n = vec->size - idx;
    memmove(vec->data + idx,
            vec->data + idx + 1,
            (vec->size - idx - n) * sizeof(ARRAY_ITEM));
    vec->size -= n;
}

#endif /* ARRAY_DEFINE */


#ifdef    ARRAY_UNDEF_DECLARE_AND_DEFINE_
# undef   ARRAY_UNDEF_DECLARE_AND_DEFINE_
# undef   ARRAY_DECLARE
# undef   ARRAY_DEFINE
#endif /* ARRAY_UNDEF_DECLARE_AND_DEFINE_ */

#ifdef    ARRAY_FUNC_DEFAULT_
# undef   ARRAY_FUNC_DEFAULT_
# undef   ARRAY_FUNC
#endif /* ARRAY_FUNC_DEFAULT_ */


#undef ARRAY_TYPE
#undef ARRAY_ITEM
#undef ARRAY_PREFIX
#undef CONCAT_
#undef CONCAT
#undef FN
