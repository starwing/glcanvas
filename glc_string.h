#ifndef glc_string_h
#define glc_string_h


struct GLCanvas;

typedef struct GLC_FontObject *GLC_Font;

GLC_Font glc_font_new(void);
GLC_Font glc_font_from_file(const char *filename, float weight);
GLC_Font glc_font_from_name(const char *name, float weight);
void     glc_font_delete(GLC_Font font);

void glc_drawstring(struct GLCanvas *c, const char *utf8, size_t len, GLC_Font font);


#endif /* glc_string_h */
