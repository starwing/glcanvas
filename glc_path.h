#ifndef glc_path_h
#define glc_path_h


#include <stddef.h>

#define GLC_PATH_ABSOLUTE 0
#define GLC_PATH_RELATIVE 1

typedef struct GLC_PathObject *GLC_Path;
typedef unsigned char GLC_PathCmd;

typedef enum GLC_CapStyle {
    GLC_CAP_BUTT = 1,
    GLC_CAP_ROUND,
    GLC_CAP_SQUARE,
    GLC_CAP_NUM_ENUM
} GLC_CapStyle;

typedef enum GLC_JoinStyle {
    GLC_JOIN_MITER = 1,
    GLC_JOIN_ROUND,
    GLC_JOIN_BEVEL,
    GLC_JOIN_NUM_ENUM
} GLC_JoinStyle;

#define GLC_CLOSE_PATH   ( 0  << 1 )
#define GLC_MOVE_TO      ( 1  << 1 )
#define GLC_LINE_TO      ( 2  << 1 )
#define GLC_HLINE_TO     ( 3  << 1 )
#define GLC_VLINE_TO     ( 4  << 1 )
#define GLC_QUAD_TO      ( 5  << 1 )
#define GLC_CUBIC_TO     ( 6  << 1 )
#define GLC_SQUAD_TO     ( 7  << 1 )
#define GLC_SCUBIC_TO    ( 8  << 1 )
#define GLC_SCCWARC_TO   ( 9  << 1 )
#define GLC_SCWARC_TO    ( 10 << 1 )
#define GLC_LCCWARC_TO   ( 11 << 1 )
#define GLC_LCWARC_TO    ( 12 << 1 )
#define GLC_PATH_NUM_COMMANDS 13

#define GLC_MOVE_TO_REL     (GLC_MOVE_TO    | GLC_PATH_RELATIVE)
#define GLC_LINE_TO_REL     (GLC_LINE_TO    | GLC_PATH_RELATIVE)
#define GLC_HLINE_TO_REL    (GLC_HLINE_TO   | GLC_PATH_RELATIVE)
#define GLC_VLINE_TO_REL    (GLC_VLINE_TO   | GLC_PATH_RELATIVE)
#define GLC_QUAD_TO_REL     (GLC_QUAD_TO    | GLC_PATH_RELATIVE)
#define GLC_CUBIC_TO_REL    (GLC_CUBIC_TO   | GLC_PATH_RELATIVE)
#define GLC_SQUAD_TO_REL    (GLC_SQUAD_TO   | GLC_PATH_RELATIVE)
#define GLC_SCUBIC_TO_REL   (GLC_SCUBIC_TO  | GLC_PATH_RELATIVE)
#define GLC_SCCWARC_TO_REL  (GLC_SCCWARC_TO | GLC_PATH_RELATIVE)
#define GLC_SCWARC_TO_REL   (GLC_SCWARC_TO  | GLC_PATH_RELATIVE)
#define GLC_LCCWARC_TO_REL  (GLC_LCCWARC_TO | GLC_PATH_RELATIVE)
#define GLC_LCWARC_TO_REL   (GLC_LCWARC_TO  | GLC_PATH_RELATIVE)


GLC_Path glc_path_new(void);
GLC_Path glc_path_from_string(const char *cmds);
void     glc_path_delete(GLC_Path path);

void glc_path_clear(GLC_Path path);
int  glc_path_concat(GLC_Path dst, GLC_Path src);
int  glc_path_append(GLC_Path dst, size_t count, const GLC_PathCmd *cmd, const float *data);
int  glc_path_appendstring(GLC_Path dst, const char *cmds);

void glc_path_bounds(GLC_Path path, float box[4]);
void glc_path_transform(GLC_Path path, float m[9]);
void glc_path_interpolate(GLC_Path path, GLC_Path start, GLC_Path stop);

void glc_path_setlinewidth(GLC_Path path, float width);
void glc_path_setcapstyle(GLC_Path path, GLC_CapStyle style);
void glc_path_setjoinstyle(GLC_Path path, GLC_JoinStyle style);
void glc_path_setmiterlimit(GLC_Path path, float limit);

int glc_path_stencil_stroke (GLC_Path path);
int glc_path_stencil        (GLC_Path path);


#endif /* glc_path_h */
