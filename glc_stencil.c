#include "glc_stencil.h"
#include <GL/GL.h>

#define STENCIL_BIT     0x1
#define MASK_BIT        0x80

static void draw_array(GLenum mode, size_t count, const float *pos) {
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(2, GL_FLOAT, 0, pos);
    glDrawArrays(mode, 0, count);
    glDisableClientState(GL_VERTEX_ARRAY);
}

static void draw_bounds(const float bounds[4]) {
    float left = bounds[0], right  = bounds[0]+bounds[2];
    float top  = bounds[1], bottom = bounds[1]+bounds[3];
    float quad[8] = {
        left,top,  left,bottom,
        right,top, right,bottom,
    };
    draw_array(GL_TRIANGLE_STRIP, 4, quad);
}

void glc_stencil_clear(void) {
    glClearStencil(0);
    glClear(GL_STENCIL_BUFFER_BIT);
}

void glc_stencil_calcbounds(float bounds[4], GLC_MaskOp op, size_t count, const float *pos) {
    enum { MIN_X, MIN_Y, MAX_X, MAX_Y };
    float bbox[4], *out = bounds;

    if (op != GLC_MASK_SET)
        bounds = bbox;

    {
        size_t i;
        const float *p;
        for (i = 0, p = pos; i < count; ++i, p += 2) {
            if (bounds[MIN_X] > p[0]) bounds[MIN_X] = p[0];
            else if (bounds[MAX_X] < p[0]) bounds[MAX_X] = p[0];
            if (bounds[MIN_Y] > p[1]) bounds[MIN_Y] = p[1];
            else if (bounds[MAX_Y] < p[1]) bounds[MAX_Y] = p[1];
        }
    }

    switch (op) {
    default:
    case GLC_MASK_SET:
        return;

    case GLC_MASK_UNION:
        if (out[MIN_X] > bounds[MIN_X]) out[MIN_X] = bounds[MIN_X];
        if (out[MAX_X] < bounds[MAX_X]) out[MAX_X] = bounds[MAX_X];
        if (out[MIN_Y] > bounds[MIN_Y]) out[MIN_Y] = bounds[MIN_Y];
        if (out[MAX_Y] < bounds[MAX_Y]) out[MAX_Y] = bounds[MAX_Y];
        return;

    case GLC_MASK_INTERSECT:
        if (out[MIN_X] < bounds[MIN_X]) out[MIN_X] = bounds[MIN_X];
        if (out[MAX_X] > bounds[MAX_X]) out[MAX_X] = bounds[MAX_X];
        if (out[MIN_Y] < bounds[MIN_Y]) out[MIN_Y] = bounds[MIN_Y];
        if (out[MAX_Y] > bounds[MAX_Y]) out[MAX_Y] = bounds[MAX_Y];
        return;
    }
}

void glc_stencil_fillbounds(float bounds[4]) {
    glStencilMask(STENCIL_BIT);
    glStencilFunc(GL_NOTEQUAL, 0, STENCIL_BIT|MASK_BIT);
    glStencilOp(GL_ZERO, GL_ZERO, GL_ZERO);

    glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
    draw_bounds(bounds);
}

void glc_stencil_draw(GLC_MaskElement e, size_t count, const float *pos) {
    GLenum elems[] = { GL_TRIANGLES, GL_TRIANGLE_FAN, GL_TRIANGLE_STRIP };
    GLenum elem = elems[0];

    if (e > 0 && e < GLC_MASK_NUM_ELEMENTS)
        elem = elems[e];

    glEnable(GL_STENCIL_TEST);
    glStencilMask(STENCIL_BIT);
    glStencilFunc(GL_NOTEQUAL, 0xFF, MASK_BIT);
    glStencilOp(GL_ZERO, GL_REPLACE, GL_REPLACE);

    glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
    draw_array(elem, count, pos);
}

void glc_stencil_polygon(size_t count, const float *pos) {
    glEnable(GL_STENCIL_TEST);
    glStencilMask(STENCIL_BIT);
    glStencilFunc(GL_NOTEQUAL, 0xFF, MASK_BIT);
    glStencilOp(GL_ZERO, GL_INVERT, GL_INVERT);

    glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
    draw_array(GL_TRIANGLE_FAN, count, pos);
}

void glc_stencil_tomask(GLC_MaskOp op, const float bounds[4]) {
    glEnable(GL_STENCIL_TEST);
    glStencilMask(MASK_BIT);

    switch (op) {
    default: break;
    case GLC_MASK_SET:
        glStencilFunc(GL_EQUAL, 0xFF, STENCIL_BIT);
        glStencilOp(GL_ZERO, GL_REPLACE, GL_REPLACE);
        break;

    case GLC_MASK_UNION:
        glStencilFunc(GL_NOTEQUAL, 0xFF, STENCIL_BIT);
        glStencilOp(GL_KEEP, GL_REPLACE, GL_REPLACE);
        break;

    case GLC_MASK_XOR:
        glStencilFunc(GL_EQUAL, 0xFF, STENCIL_BIT);
        glStencilOp(GL_KEEP, GL_INVERT, GL_INVERT);
        break;

    case GLC_MASK_INTERSECT:
        glStencilFunc(GL_EQUAL, STENCIL_BIT, STENCIL_BIT);
        glStencilOp(GL_ZERO, GL_KEEP, GL_KEEP);
        break;

    case GLC_MASK_SUBTRACT:
        glStencilFunc(GL_EQUAL, STENCIL_BIT, STENCIL_BIT);
        glStencilOp(GL_KEEP, GL_ZERO, GL_ZERO);
        break;
    }

    draw_bounds(bounds);
    glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
}
